'''
import text
return average word length, 
    longest word, 
    shortest word, 
    most common words
regular first then multithreading
'''
import time
import threading
#read text
def read_text(txt):

    words = []

    #for threading purposes
    for line in txt:
        words += line.split()

    return words

#average word length
def avg_word_length(txt):

    all_lengths = 0

    for item in txt:
        all_lengths += len(item)
    
    avg_length = all_lengths/len(txt)

    return avg_length

#longest word
def longest_word(txt):

    long_word = txt[0]

    for item in txt:
        time.sleep(2)
        if (len(item) > len(long_word)):
            long_word = item

    return long_word

#most common word
def most_common_word(txt):
    
    common_word = txt[0]

    word_dictionary = {}
    
    for item in txt:
        if item not in word_dictionary:
            word_dictionary[item] = 1
        else:
            word_dictionary[item] += 1

    for item in word_dictionary:
        common_word
        if word_dictionary[item] > word_dictionary[common_word]:
            common_word = item
    
    return common_word

def analyze(text):
    print('Average word length: ', avg_word_length(text))
    print('Longest word: ', longest_word(text))
    print('Most common word: ', most_common_word(text))
    
def analyze_multi_thread(text):
    x1 = threading.Thread(target=avg_word_length, args=(text,))
    

    x2 = threading.Thread(target=longest_word, args=(text,))
 

    x3 = threading.Thread(target=most_common_word, args=(text,))

    x1.start()
    x2.start()
    x3.start()


#analysis??
if __name__ == "__main__":
    f = open("sample.txt", "r")
    text = read_text(f)

    start = time.time()

    analyze(text)

    print("Time executed: ", time.time() - start)
    
    start = time.time()

    analyze_multi_thread(text)
    print(threading.active_count())
    print("Time executed using multithread: ", time.time() - start)
